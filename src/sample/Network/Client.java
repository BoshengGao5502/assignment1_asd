package sample.Network;

import java.io.*;
import java.net.Socket;

public class Client {

    public static String SERVER_IP; //IP
    public static int SERVER_PORT;

    public void connect() throws IOException, InterruptedException {
            try(Socket clientSocket = new Socket(SERVER_IP, SERVER_PORT)) {


                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));


                DataInputStream input = new DataInputStream(clientSocket.getInputStream());
                DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());

                System.out.println("The client joined the server. Port: " + Server.SERVER_PORT);


                while (!clientSocket.isOutputShutdown()) {
                    if (bufferedReader.ready()) {
                        System.out.println("The client started recording the data into the buffer.");
                        String clientData = bufferedReader.readLine();
                        output.writeUTF(clientData);
                        output.flush();
                        System.out.println("The client sent the data: "  + clientData);
                        //ставим поток на паузу, чтобы сервер успел прочитать данные
                        Thread.sleep(2000);

                        if (clientData.equalsIgnoreCase("exit")) {
                            System.out.println("The client broke the compound.");

                            Thread.sleep(2000);


                            checkServerData(input);
                            output.close();
                            break;
                        }
                        System.out.println("The client sent the data and waiting for a response from the server.");

                        Thread.sleep(2000);
                        checkServerData(input);
                    }
                }
                System.out.println("The client's disconnected. Close all channels.");
                input.close();
                System.out.println("All client channels are closed.");


            }
    }

    public static void checkServerData(DataInputStream input) throws IOException {
        System.out.println("The client reads data from the server.");
        String serverInput = input.readUTF();
        System.out.println(serverInput);
        System.out.println();
    }
}
