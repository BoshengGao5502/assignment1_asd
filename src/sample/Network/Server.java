package sample.Network;

import javafx.application.Platform;
import sample.AlertWindow;

import javax.sound.midi.Soundbank;
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Server {

    public static final int SERVER_PORT = 1337;

    public static void run() throws IOException, InterruptedException {

        ServerSocket serverSocket = new ServerSocket(SERVER_PORT);

        Runnable ipAndPortWindowShow = () -> {
            try {
                AlertWindow ipAndPortInfoWindow = new AlertWindow();
                ipAndPortInfoWindow.display(
                        "IP & Port Info",
                        "Your IP: " + Inet4Address.getLocalHost().getHostAddress() + "\n" +
                                "Your Port: 1337",
                        200,
                        200,
                        200,
                        200
                );
            } catch (UnknownHostException exception) {
                exception.printStackTrace();
            }
        };
        Platform.runLater(ipAndPortWindowShow);
        System.out.println("Server set up. Waiting for connection...");

        while (!serverSocket.isClosed()) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));


            Socket clientSocket = serverSocket.accept();
            System.out.println("The client's connected.");




            DataInputStream input = new DataInputStream(clientSocket.getInputStream());
            System.out.println("Data Input Stream created");
            DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
            System.out.println("Data Output Stream created");
            System.out.println();

            while (!clientSocket.isClosed()) {
                System.out.println("Server reads data from the channel...");
                String entryData = input.readUTF();
                System.out.println("Server read: " + entryData);

                if (entryData.equalsIgnoreCase("exit")) {
                    System.out.println("Server sends response: " + entryData);
                    output.writeUTF("The server replies: yes, I hear you. " + entryData);
                    output.flush();
                    System.out.println("Users disconnect.");


                    Thread.sleep(3000);
                    input.close();
                    break;
                }
                System.out.println("The server response, yes, I did." + entryData);
                System.out.println();
                output.writeUTF("Server response: Yes, I heard " + entryData);
                output.flush();

            }
            System.out.println("Close all channels.");
            input.close();
            output.close();
        }
        System.out.println("Server's off. \n" +
                "All connections and channels have been successfully closed!");

    }
}






