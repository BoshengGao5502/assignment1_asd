package sample.Network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class RunnableClientTester implements Runnable {

    private Socket clientSocket;

    public RunnableClientTester() {
        try {
            clientSocket = new Socket("localhost", MultiThreadServer.SERVER_PORT);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

    }
    @Override
    public void run() {
        try {

            DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
            System.out.println("Location Channel Settings");
            DataInputStream input = new DataInputStream(clientSocket.getInputStream());
            System.out.println("The Reading Channel is created");


            for (int i = 0; i < 5; i++) {
                output.writeUTF("clientCommand " + i + 1);
                output.flush();
                Thread.sleep(100);
                System.out.println("The client sent the team." + i + 1);

                System.out.println("The client is waiting for a response from the server....");
                Thread.sleep(1000);
                String entryData = input.readUTF();
                System.out.println("Server response: " + entryData);
            }

        }
        catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}
