package sample.Network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler implements Runnable {

    private Socket clientSocket;

    ClientHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        try {

            DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
            System.out.println("The recording channel has been created.");


            DataInputStream input = new DataInputStream(clientSocket.getInputStream());
            System.out.println("The Reading Channel is created");

            System.out.println();


            while (!clientSocket.isClosed()) {
                System.out.println("The server started reading the channel.");


                String entryData = input.readUTF();


                if (entryData.equalsIgnoreCase("exit")) {
                    System.out.println("The client's off.");
                    output.writeUTF("Server : I hope you won't send next time. " + entryData);
                    output.flush();
                    Thread.sleep(2000);
                    input.close();
                    break;
                }


                output.writeUTF("Server response: Yes, I heard " + entryData);
                System.out.println("The server sent the reply to the client: Yes, I heard. " + entryData);


                output.flush();
                System.out.println();
            }

            System.out.println("Close all communication channels....");
            input.close();
            System.out.println("The reading channel is closed.");
            output.close();
            System.out.println("The record is closed.");
        }
        catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}
